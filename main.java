import java.util.*;
public class main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x = 1;
        int [] arr1 = {1,2,3};
        do {
            try {
                System.out.println("Enter position in array you want: ");
                int n1 = input.nextInt();
                System.out.println(arr1[n1-1]);
                x=2;

            } catch (Exception e) {
                System.out.println("This is why QA Engineers always have to do boundary testing! The array only has 3 values and you've requested a 4th");
            }
        } while (x == 1);
    }
}



